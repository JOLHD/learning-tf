provider "aws" {
  region = "us-east-1"
  access_key = "AKIAU7VJ3VKLDCEFLHOD"
  secret_key = "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"
}

variable "cidr_blocks" {
  description = "cidr blocks for vpc and subnet"
  type = list(string)
}

variable "environment" {
  description = "development environment"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    Name = var.environment
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.cidr_blocks[1]
  availability_zone = "us-east-1a"
  tags = {
    Name = "dev-subnet-1"
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing_vpc.id
  cidr_block = "172.31.128.0/20"
  availability_zone = "us-east-1a"
  tags = {
    Name = "default-subnet-2"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}